package my.scalafx

import scalafx.application.JFXApp3
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.{Alert, Button, Label, RadioButton, TextField, ToggleGroup}
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint._
import scalafx.scene.text.Text

object TemperatureConverterGUI extends JFXApp3 {
  override def start(): Unit = {
    stage = new JFXApp3.PrimaryStage {
      //    initStyle(StageStyle.Unified)
      title = "ScalaFX Temperature Converter"
      scene = new Scene(750, 400) {
        fill = Color.rgb(38, 38, 38)
        private val heading = new HBox {
          padding = Insets(50, 80, 50, 80)
          children = Seq(
            new Text {
              text = "Temperature Converter"
              style = "-fx-font: normal bold 40pt sans-serif"
              fill = new LinearGradient(
                endX = 0,
                stops = Stops(Yellow, Orange))
              effect = new DropShadow {
                color = DarkGray
                radius = 10
                spread = 0.1
              }
            },
          )
        }

        private val rb1 = new RadioButton {
          text = "C -> F"
          layoutX = 295
          layoutY = 150
          textFill = White
          selected = true // Default state
        }

        private val rb2 = new RadioButton {
          text = "F -> C"
          layoutX = 395
          layoutY = 150
          textFill = White
        }

        // Allows only one radio button to be selected at the time
        private val toggleGroup = new ToggleGroup {
          toggles = List(rb1, rb2)
        }

        private val text1 = new Label {
          text = "Celsius"
          layoutX = 150
          layoutY = 200
          textFill = White
        }

        private val textField1 = new TextField {
          layoutX = 150
          layoutY = 220
          promptText = "Temperature in Celsius" // Placeholder for Celsius text field
        }

        private val text2 = new Label {
          text = "Fahrenheit"
          layoutX = 450
          layoutY = 200
          textFill = White
        }

        private val textField2 = new TextField {
          layoutX = 450
          layoutY = 220
          promptText = "Temperature in Fahrenheit" // Placeholder for Fahrenheit text field
        }

        private val btnCalculate = new Button {
          text = "Calculate"
          tooltip = "Click to calculate C -> F" // Tooltip when hoover over the button
          layoutX = 332
          layoutY = 300
          style = "-fx-font: 15 arial; -fx-background-radius: 10; -fx-base: #ffcc00;" // Button style
        }

        content = List(heading, rb1, rb2, text1, textField1, text2, textField2, btnCalculate)


        btnCalculate.setOnAction(_ => {
          try {
            if (rb1.isSelected) {
              val cel = textField1.getText.toDouble
              var far = BigDecimal((cel * 1.8) + 32)
              far = far.setScale(2, BigDecimal.RoundingMode.HALF_EVEN)
              textField2.setText(far.toString)
            } else {
              val far = textField2.getText.toDouble
              var cel = BigDecimal((far - 32) * 0.5556)
              cel = cel.setScale(2, BigDecimal.RoundingMode.HALF_EVEN)
              textField1.setText(cel.toString)
            }
          } catch {
            case _: NumberFormatException =>
              // Shows alert in case of invalid input
              new Alert(AlertType.Information) {
                title = "Invalid input"
                headerText = "Conversion is not possible."
                contentText = "Input must be a number."
              }.showAndWait()
          }
        })
      }
    }
  }
}
